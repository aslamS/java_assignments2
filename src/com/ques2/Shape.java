package com.ques2;

public abstract class Shape {
	public abstract void areaOfRectangle(double l, double b);

	public abstract void areaOfSquare(double side);

	public abstract void areaOfCircle(double r);

}
