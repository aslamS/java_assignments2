package com.ques2;

public class Questiond {
	public static void main(String[] args) {
		areaOfRectangle(5, 8);
		areaOfSquare(7);
		final double pi = 3.14;
		areaOfCircle(pi, 6.7);

	}

	private static void areaOfCircle(double pi, double r) {
		double x = pi * r * r;
		System.out.println("Area of Circle=" + x);

	}

	private static void areaOfSquare(double a) {
		double y = a * a;
		System.out.println("Area of Square=" + y);

	}

	private static void areaOfRectangle(double l, double b) {
		double z = l * b;
		System.out.println("Area of Rectangle=" + z);

	}

}
