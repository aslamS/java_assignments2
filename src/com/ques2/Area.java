package com.ques2;

public class Area extends Shape {

	@Override
	public void areaOfRectangle(double l, double b) {
		double z = l * b;
		System.out.println("Area of Rectangle=" + z);

	}

	@Override
	public void areaOfSquare(double side) {
		double y = side * side;
		System.out.println("Area of Square=" + y);

	}

	@Override
	public void areaOfCircle(double r) {
		final double pi = 3.14;
		double x = pi * r * r;
		System.out.println("Area of Circle=" + x);

	}

	public static void main(String[] args) {
		Area a1 = new Area();
		a1.areaOfRectangle(3, 4);
		a1.areaOfSquare(5);
		a1.areaOfCircle(5);
	}

}
