package com.ques2;

public class SmartTelephone extends Telephone {
	@Override
	public void with() {
		System.out.println("with");

	}

	@Override
	public void lift() {
		System.out.println("lift");

	}

	@Override
	public void disconnected() {
		System.out.println("disconnected");

	}

	public static void main(String[] args) {
		SmartTelephone st = new SmartTelephone();
		st.lift();
		st.disconnected();
		st.with();
	}

}
