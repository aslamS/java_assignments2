package com.abstractclass;

public class Car implements Santro {
	public void drive() {
		System.out.println("drive");

	}

	public void stop() {
		System.out.println("stop");

	}

	@Override
	public void gearchange() {
		System.out.println("gearchange");

	}

	@Override
	public void music() {
		System.out.println("music");

	}

	public static void main(String[] args) {
		Car c = new Car();
		c.drive();
		c.stop();
		c.gearchange();
		c.music();

	}
}
