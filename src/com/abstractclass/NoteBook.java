package com.abstractclass;

public class NoteBook extends Book {
	public void draw() {
		System.out.println("draw method");

	}

	@Override
	public void write() {
		System.out.println("write");

	}

	@Override
	public void Read() {
		System.out.println("read");

	}

//	public static void main(String[] args) {
//		NoteBook n = new NoteBook();
//		n.draw();
//		n.write();
//		n.Read();
////	
}
