package com.test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListArray {
	public static void main(String[] args) {
		List<String> l = new ArrayList<String>();
		//List<String> l = new LinkedList<String>();
		l.add("Bovonto");
		l.add("Coke");
		l.add("maaza");
		l.add("pepsi");
		l.add("limca");
		System.out.println("size of the list " + l.size());
		System.out.println("content of the list is : " + l);
		// removing second index
		l.remove(1);

		System.out.println("after removed :" + l);
//		boolean contains = l.contains("coke");
//		System.out.println(contains);
//		
		if (l.equals("Coke")) {
			System.out.println("coke is  present");
		} else {
			System.out.println("coke is not  present");
		}
	}
}
